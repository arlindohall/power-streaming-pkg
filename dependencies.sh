#!/bin/bash
# dependencies.sh
# August 2016
# Miller Hall
# Install Flink dependencies and set up the enviornment with:
#     collectd
#     zookeeper
#     kafka
#     flink
#     jobmanager
#     example program

CUR_DIR=$(pwd)

# Install Collectd
sudo yum install epel-release
sudo yum install collectd
sudo cp collecd.conf /etc/collectd.conf

# Install librdkafka
# If you don't have the rpm enable the following line:
# wget http://rpms.famillecollet.com/SRPMS/librdkafka-0.9.1-1.el7.remi.src.rpm
sudo yum install librdkafka-0.9.1-1.el7.remi.x86_64.rpm 

# Install Gradle
unzip gradle-2.14.1-all.zip
mv gradle-2.14.1 gradle
sudo mv gradle /opt/
cp kafka.sh /etc/profile.d/

# Install Kafka
tar xfv kafka-0.10.0.0-src.tgz
mv kafka-0.10.0.0-src kafka
sudo mv kafka /opt/

# Install Flink
# The compiled java is included
# The following should not be necessary
# cd flink
# mvn clean package

# cd $CUR_DIR
