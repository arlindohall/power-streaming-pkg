/******************************************************************************/
/* NAME: power.c                                                              */
/* DATE: May 2016                                                             */
/* AUTH: Miller Hall                                                          */
/*          mhall6@g.clemson.edu                                              */
/* PURP: Plugin for collectd which reports energy stats on intel x86_64       */
/*          architectures, versions later than sandy_bridge                   */
/* LISC: GPLv2                                                                */
/******************************************************************************/

/* rapl_read
 * Vince Weaver
 * vincent.weaver@main.edu
 * Sept 2015
 * Read RAPL registers on >= sandybridge Intel procs
 * For source, see:
 *      http://web.eece.maine.edu/~vweaver/projects/rapl/rapl-read.c
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <inttypes.h>
#include <unistd.h>
#include <math.h>
#include <string.h>

#include <sys/syscall.h>
#include <linux/perf_event.h>

#include "power.h"

#define debug_flag
void debug_print(const char msg[])
{
#ifdef debug_flag
    FILE *fpt;
    static int n = 0;

    fpt = fopen("/home/millerhall/git/collectd/debug.txt", "a");
    fprintf(fpt, "Tag %d: %s\n", n, msg);
    fclose(fpt);
    n++;
#endif
    return;
}

static int open_msr(int core)
{
        char msr_filename[BUFSIZ];
        int fd;

        sprintf(msr_filename, "/dev/cpu/%d/msr", core);
        fd = open(msr_filename, O_RDONLY);
        if ( fd < 0 ) {
                if ( errno == ENXIO ) {
                        fprintf(stderr, "rdmsr: No CPU %d\n", core);
                        exit(2);
                } else if ( errno == EIO ) {
                        fprintf(stderr, "rdmsr: CPU %d doesn't support MSRs\n",
                                        core);
                        exit(3);
                } else {
                        perror("rdmsr:open");
                        fprintf(stderr,"Trying to open %s\n",msr_filename);
                        exit(127);
                }
        }
        return fd;
}

static long long read_msr(int fd, int which)
{
        uint64_t data;

        if ( pread(fd, &data, sizeof data, which) != sizeof data ) {
                perror("rdmsr:pread");
                exit(127);
        }
        return (long long)data;
}

static int detect_cpu(void)
{
        FILE *fff;
        int family,model=-1;
        char buffer[BUFSIZ],*result;
        char vendor[BUFSIZ];

        fff=fopen("/proc/cpuinfo","r");
            if (fff==NULL) return -1;
        while(1) {
                result=fgets(buffer,BUFSIZ,fff);
                    if (result==NULL) break;
                if (!strncmp(result,"vendor_id",8)) {
                    sscanf(result,"%*s%*s%s",vendor);
                    if (strncmp(vendor,"GenuineIntel",12)) {
                            printf("%s not an Intel chip\n",vendor);
                            return -1;
                    }
                }
                if (!strncmp(result,"cpu family",10)) {
                    sscanf(result,"%*s%*s%*s%d",&family);
                    if (family!=6) {
                            printf("Wrong CPU family %d\n",family);
                            return -1;
                    }
                }
                if (!strncmp(result,"model",5)) {
                        sscanf(result,"%*s%*s%d",&model);
                }
        }
        fclose(fff);

        switch(model) {
                case CPU_SANDYBRIDGE:
                case CPU_SANDYBRIDGE_EP:
                case CPU_IVYBRIDGE:
                case CPU_IVYBRIDGE_EP:
                case CPU_HASWELL:
                case CPU_HASWELL_EP:
                case CPU_BROADWELL:
                        break;
                default:
                        model=-1;
                        break;
        }

        return model;
}

static int total_cores = 0;
static int total_packages = 0;
static int package_map[MAX_PACKAGES];

#define PACKAGE_FILENAME "/sys/devices/system/cpu/cpu%d/topology/physical_package_id"
static int detect_packages(void)
{
        char filename[BUFSIZ];
        FILE *fff;
        int package;
        int result;
        int i;

        for(i = 0; i < MAX_PACKAGES; i++) package_map[i] = -1;

        printf("\t");
        for(i=0;i<MAX_CPUS;i++) {
                sprintf(filename, PACKAGE_FILENAME, i);
                fff = fopen(filename, "r");
                if (fff == NULL) break;
                    result = fscanf(fff, "%d", &package);
                if (result == 0)
                    exit(1);
                fclose(fff);
                if (package_map[package] == -1) {
                        total_packages++;
                        package_map[package] = i;
                }
        }

        total_cores = i;
        return 0;
}

// RAPL Data
static rapl_t   rd;
static int      cpu_model;

/*******************************/
/* MSR code                    */
/*******************************/
static int rapl_msr(int core)
{
        int fd, j;
        long long result;
        static int firstTime = 1;

        if (cpu_model < 0) {
                return -1;
        }

        for(j=0; j < total_packages; j++) {
            fd = open_msr(package_map[j]);

            if (firstTime) {
                // Units will not change between rapl_msr calls
                /* Calculate the units used */
                result = read_msr(fd, MSR_RAPL_POWER_UNIT);

                rd.power_units = pow(0.5, (double) (result & 0xf));
                rd.cpu_energy_units[j] = pow(0.5, (double) ((result >> 8) & 0x1f));
                rd.time_units = pow(0.5, (double) ((result >> 16) & 0xf));

                /* On Haswell EP the DRAM units differ from the CPU ones */
                if (cpu_model == CPU_HASWELL_EP) {
                        rd.dram_energy_units[j] = pow(0.5, (double) 16);
                }
                else {
                        rd.dram_energy_units[j] = rd.cpu_energy_units[j];
                }

                /* Show package power info */
                result = read_msr(fd, MSR_PKG_POWER_INFO);
                rd.thermal_spec_power = rd.power_units * (double) (result & 0x7fff);
                rd.minimum_power = rd.power_units * (double) ((result >> 16) & 0x7fff);
                rd.maximum_power = rd.power_units * (double) ((result >> 32) & 0x7fff);
                rd.time_window = rd.time_units * (double) ((result >> 48) & 0x7fff);

                firstTime = 0;
            }

            /* Show package power limit */
            result = read_msr(fd, MSR_PKG_RAPL_POWER_LIMIT);
            rd.power_limit_lock = (result >> 63);
            rd.pkg_power_limit_1 = rd.power_units * (double) ((result >> 0) & 0x7FFF);
            rd.pkg_time_window_1 = rd.time_units * (double) ((result >> 17) & 0x007F);
            rd.pkg_limit_1_enable = (result & (1LL << 15));
            rd.pkg_limit_1_clamp = (result & (1LL << 16));
            rd.pkg_power_limit_2 = rd.power_units * (double) ((result >> 32) & 0x7FFF);
            rd.pkg_time_window_2 = rd.time_units * (double) ((result >> 49) & 0x007F);
            rd.pkg_limit_2_enable = (result & (1LL << 47));
            rd.pkg_limit_2_clamp = (result & (1LL << 48));

            /* Package throttled time */
            /* only available on *Bridge-EP */
            // Defined as time going below os-requested p-state or t-state
            //   see programming guide ch 14-35, ch 35
            if ((cpu_model == CPU_SANDYBRIDGE_EP) || (cpu_model == CPU_IVYBRIDGE_EP)) {
                    result = read_msr(fd, MSR_PKG_PERF_STATUS);
                    rd.acc_pkg_throttled_time = (double) result * rd.time_units;
            }

            /* PP0 throttled time and priority_level */
            /* only available on *Bridge-EP */
            if ((cpu_model == CPU_SANDYBRIDGE_EP) || (cpu_model == CPU_IVYBRIDGE_EP)) {
                    result = read_msr(fd, MSR_PP0_PERF_STATUS);
                    rd.acc_pp0_throttled_time = (double) result * rd.time_units;

                    result = read_msr(fd, MSR_PP0_POLICY);
                    rd.pp0_priority_level = (int) result & 0x001f;
            }

            /* PP1 priority_level */
            if ((cpu_model == CPU_SANDYBRIDGE) || (cpu_model == CPU_IVYBRIDGE) ||
                    (cpu_model == CPU_HASWELL) || (cpu_model == CPU_BROADWELL)) {

                    result = read_msr(fd,MSR_PP1_POLICY);
                    rd.pp1_priority_level = (int) result & 0x001f;
            }
            close(fd);
        }

        for(j = 0; j < total_packages; j++) {
            fd = open_msr(package_map[j]);

            /* Package Energy */
            result = read_msr(fd, MSR_PKG_ENERGY_STATUS);
            rd.package_energy[j] = (double) result * rd.cpu_energy_units[j];

            /* PP0 energy */
            /* Not available on Haswell-EP? */
            result = read_msr(fd, MSR_PP0_ENERGY_STATUS);
            rd.pp0_energy[j] = (double) result * rd.cpu_energy_units[j];

            /* PP1 energy */
            /* not available on *Bridge-EP */
            if ((cpu_model == CPU_SANDYBRIDGE) || (cpu_model == CPU_IVYBRIDGE) ||
                    (cpu_model == CPU_HASWELL) || (cpu_model == CPU_BROADWELL)) {

                    result = read_msr(fd, MSR_PP1_ENERGY_STATUS);
                    rd.pp1_energy[j] = (double)result * rd.cpu_energy_units[j];
            }
            /* Updated documentation (but not the Vol3B) says Haswell and   */
            /* Broadwell have DRAM support too                              */
            if ((cpu_model == CPU_SANDYBRIDGE_EP) || (cpu_model == CPU_IVYBRIDGE_EP) ||
                    (cpu_model==CPU_HASWELL_EP) ||
                    (cpu_model == CPU_HASWELL) || (cpu_model == CPU_BROADWELL)) {

                    result = read_msr(fd, MSR_DRAM_ENERGY_STATUS);
                    rd.dram_energy[j] = (double)result * rd.dram_energy_units[j];
            }
            close(fd);
        }
        return 0;
}

/* perf_count
 * Miller Hall
 * mhall6@g.clemson.edu
 * May 2016
 * Module of plugin power which handles perf counter msr's
 */

/* power plugin functions
 * Miller Hall
 * mhall6@g.clemson.edu
 * May 2016
 * Plugin for collectd to report power information
 */

#include "collectd.h"
#include "common.h"
#include "plugin.h"

static const char *config_keys[] =
{
    "RAPL"
};
static int config_keys_num = STATIC_ARRAY_SIZE(config_keys);

// Register flags
static int rapl = 0;

// Architecture flags

static int power_config(const char *key, const char *value)
{
    debug_print("power_config");
    // Determine desired registers
    if (strcasecmp(key, "RAPL") == 0 && IS_TRUE(value))
        rapl = 1;

    return 0;
}

static int power_init(void)
{
    debug_print("power_init");
    // Detect Architecture
    int i;

    cpu_model = detect_cpu();
    detect_packages();

    for (i = 0; i < MAX_PACKAGES; i++) {
        rd.package_energy[i] = 0;
        rd.pp0_energy[i] = 0;
        rd.pp1_energy[i] = 0;
        rd.dram_energy[i] = 0;
    }

    // Turn off impossible registers regardless of config
    return 0;
}

static int power_rapl_energy_submit(int package_num)
{
    char plugin_inst[256];
    value_t energy[4];
    value_list_t vl = VALUE_LIST_INIT;

    energy[0].gauge = rd.package_energy[package_num];
    energy[1].gauge = rd.pp0_energy[package_num];
    energy[2].gauge = rd.pp1_energy[package_num];
    energy[3].gauge = rd.dram_energy[package_num];

    vl.values = energy;
    vl.values_len = STATIC_ARRAY_SIZE(energy);

    sprintf(plugin_inst, "%d", package_num);

    sstrncpy (vl.host, hostname_g, sizeof (vl.host));
    sstrncpy (vl.plugin, "power", sizeof (vl.plugin));
    sstrncpy (vl.plugin_instance, plugin_inst, sizeof (vl.plugin));
    sstrncpy (vl.type, "power_rapl_energy", sizeof(vl.type));

    plugin_dispatch_values(&vl);

    return 0;
}

static int power_read(void)
{
    debug_print("power_read");
    int core;
    int result = -1;
    int package;

    for (core = 0; core < total_cores; core++) {
        result = rapl_msr(core);
        if (result != 0) return result;
    }

    if (rapl) {
        for (package = 0; package < total_packages; package++) {
            power_rapl_energy_submit(package);
        }
    }

    return result;
}

void module_register(void)
{
    plugin_register_config("power", power_config, config_keys, config_keys_num);
    plugin_register_init("power", power_init);
    plugin_register_read("power", power_read);
}
/* vim :set ts=8 sts=4 sw=4 noexpandtab */
/*     :retab                           */
