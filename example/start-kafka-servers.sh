#!/bin/bash

nohup sudo /opt/kafka/bin/zookeeper-server-start.sh \
	/opt/kafka/config/zookeeper.properties &
sleep 5

nohup sudo /opt/kafka/bin/kafka-server-start.sh \
	/opt/kafka/config/server.properties &
sleep 5

nohup ../flink/build-target/bin/jobmanager.sh start local
sleep 5

#./flink run target/kafka-example-1.0-SNAPSHOT.jar \
#	--group.id myGroup --topic collectd \
#	--bootstrap.servers localhost:9092 \
#	--zookeeper.connect localhost:2181

