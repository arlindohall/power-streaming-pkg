#!/bin/bash
# run-example.sh
# Miller Hall
# August 2016
# Run the flink example

./flink run target/kafka-example-1.0-SNAPSHOT.jar \
	--group.id myGroup --topic collectd \
	--bootstrap.servers localhost:9092 \
	--zookeeper.connect localhost:2181

